## Minimal configuration requirements

### [SROS] Configure NTP [MD-CLI example]

    /configure system time zone { standard name cet }
    /configure system time dst-zone "CEST" { end week last }
    /configure system time dst-zone "CEST" { end month october }
    /configure system time dst-zone "CEST" { end hours-minutes "03:00" }
    /configure system time dst-zone "CEST" { start week last }
    /configure system time dst-zone "CEST" { start month march }
    /configure system time dst-zone "CEST" { start hours-minutes "02:00" }
    /configure system time ntp { admin-state enable }
    /configure system time ntp { server 138.203.19.135 router-instance "management" prefer true }

### [SROS] Enable gRPC [MD-CLI example]

    /configure system { grpc admin-state enable }
    /configure system { grpc allow-unsecure-connection }

### [SROS] Add user with gRPC access [MD-CLI example]

	/configure global
	/configure system security user-params local-user password { complexity-rules allow-user-name true }
	commit
	/configure system security user-params local-user user "nokia" { password @nokia123 }
	/configure system security user-params local-user user "nokia" { access grpc true }
	/configure system security user-params local-user user "nokia" { console member ["administrative"] }
	commit

## Useful examples

### Single XPATH subscription

    gnmic -a sros.node.ipd.lab:57400 -u nokia -p @nokia123 --insecure sub --path "/state/port[port-id=*]/statistics/out-octets"
    gnmic -a sros.node.ipd.lab:57400 -u nokia -p @nokia123 --insecure sub --path /state/router[router-name=Base]/bgp/neighbor[ip-address=*]/statistics/session-state

### Bulk subscription

    gnmic --config gnmic_sub_sros_sec.yaml subscribe
    gnmic --config gnmic_sub_sros_unsec.yaml subscribe

### SROS verification

    SROS# show system telemetry grpc subscription
    SROS# show system telemetry grpc subscription <id> paths

### XPath can be obtained from CLI

    [/state port 1/1/c1/1 ethernet]
    A:admin@SROS# pwc gnmi-path
    Present Working Context:
    /state/port[port-id=1/1/c1/1]/ethernet
