## Quick intro

- Main focus is gNMI, Netconf, Telemetry streaming, TPG stack (Telegraf/Prometheus/Grafana visualisation)
- Other protocols are lightly covered for completenes
- Each protocol has a dedicated directory with README file and examples
  - [Grafana](Grafana/)
  - [gNMI_SROS](gNMI_SROS/)
  - [gNMI_SRLinux](gNMI_SRLinux/)
  - [NETCONF_SROS](NETCONF_SROS/)
  - [Ansible_SROS](Ansible_SROS/)
  - [Ansible_SRLinux](Ansible_SRLinux/)
  - [SNMP_SROS](SNMP/)

> Note: All scripts were tested on Ubuntu 20.04. I don't exepct any issues to execute them on any other Linux distribution with a minor modifications.

> Note: Regular updates are not guaranteed!
