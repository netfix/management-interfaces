## Quick intro

Main focus is SROS based platforms and no plans for SRLinux examples so far.

## Recommended SROS configuration

### Classic CLI

    /config system netconf auto-config-save
    /config system netconf no shutdown
    /config system management-interface yang-modules nokia-combined-modules
    /config system security
        user "netconf"
            password @nokia123
            access netconf
            console
                no member "default"
                member "administrative"
            exit
        exit
    /configure system security profile "administrative" netconf base-op-authorization kill-session
    /configure system security profile "administrative" netconf base-op-authorization lock

### MD-CLI

    /configure system { management-interface netconf admin-state enable }
    /configure system { management-interface netconf auto-config-save true }
    /configure system management-interface yang-modules nokia-combined-modules true
    /configure system security user-params local-user user "netconf" password @nokia123
    /configure system security user-params local-user user "netconf" access { netconf true }
    /configure system security user-params local-user user "netconf" console { member ["administrative"] }
    /configure system security aaa local-profiles profile "administrative" netconf base-op-authorization { kill-session true }
    /configure system security aaa local-profiles profile "administrative" netconf base-op-authorization { lock true }

### SROS XML hints

A few tricks in SROS CLI could make life easier. This could be very useful in XML templating for automation platforms, based on NETCONF protocol

- Uncommited configuration could be presented as XML RPC using 'compare summary netconf-rpc' command.

        # compare summary netconf-rpc
        <rpc message-id="1" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:yang="urn:ietf:params:xml:ns:yang:1">
            <edit-config>
                <target><candidate/></target>
                <config>
                    <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf" xmlns:nokia-attr="urn:nokia.com:sros:ns:yang:sr:attributes">
                        <router>
                            <router-name>Base</router-name>
                            <interface>
                                <interface-name>new_interface</interface-name>
                                <port>lag-1:1</port>
                                <ipv4>
                                    <primary>
                                        <address>10.0.1.1</address>
                                        <prefix-length>24</prefix-length>
                                    </primary>
                                </ipv4>
                            </interface>
                            <bgp>
                                <neighbor>
                                    <ip-address>10.0.1.2</ip-address>
                                    <group>GRP-IBGP</group>
                                    <peer-as>1</peer-as>
                                </neighbor>
                            </bgp>
                        </router>
                    </configure>
                </config>
            </edit-config>
        </rpc>

- Existing onfiguration could be presented as XML RPC using 'info full-context xml' command

        # info full-context xml
        <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf" xmlns:nokia-attr="urn:nokia.com:sros:ns:yang:sr:attributes">
            <router>
                <router-name>Base</router-name>
                <bgp>
                    <neighbor>
                        <ip-address>172.31.22.1</ip-address>
                        <group>eBGPv4-EPE</group>
                        <peer-as>64503</peer-as>
                    </neighbor>
                </bgp>
            </router>
        </configure>

## Install NETCONF toolset

> Check: https://pypi.org/project/netconf-console2/

    apt install python3-pip
    pip3 install netconf-console2

## Examples

### Basic operations

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --hello
    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --get-config

### GET BGP configuration for GRT

```sh
cat > get-conf-bgp.xml <<EOF
<get-config>
    <source>
        <running/>
    </source>
    <filter>
    <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf" xmlns:nokia-attr="urn:nokia.com:sros:ns:yang:sr:attributes">
        <router>
            <router-name>Base</router-name>
            <bgp>
            </bgp>
        </router>
    </configure>
    </filter>
</get-config>
EOF
```

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --rpc=get-conf-bgp.xml
        <...snip>

### Add BGP peer

```sh
cat > set-conf-bgp-peer.xml <<EOF
<edit-config>
  <target>
    <candidate/>
  </target>
  <config>
    <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf" xmlns:nokia-attr="urn:nokia.com:sros:ns:yang:sr:attributes">
        <router>
            <router-name>Base</router-name>
            <bgp>
                <neighbor>
                    <ip-address>172.31.22.5</ip-address>
                    <group>eBGPv4-EPE</group>
                    <peer-as>64505</peer-as>
                </neighbor>
            </bgp>
        </router>
    </configure>
  </config>
</edit-config>
EOF
```

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --rpc=set-conf-bgp-peer.xml
        <?xml version='1.0' encoding='UTF-8'?>
        <rpc-reply xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="urn:uuid:fd64c7a3-23fa-4c1a-ae42-7c0a
            <ok/>
        </rpc-reply>

    *[gl:/configure router "Base" bgp]
    SROS# compare
    +   neighbor "172.31.22.5" {
    +       group "eBGPv4-EPE"
    +       peer-as 64505
    +   }

### [IMPORTANT] Commit changes

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --commit
        <?xml version='1.0' encoding='UTF-8'?>
        <ok xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0"/>

    SROS# /show router bgp neighbor "172.31.22.5"

### DELETE BGP peer

```sh
cat > del-conf-bgp-peer.xml <<EOF
<edit-config>
  <target>
    <candidate/>
  </target>
  <config>
    <configure xmlns="urn:nokia.com:sros:ns:yang:sr:conf" xmlns:nokia-attr="urn:nokia.com:sros:ns:yang:sr:attributes">
        <router>
            <router-name>Base</router-name>
            <bgp>
                <neighbor operation="delete">
                    <ip-address>172.31.22.5</ip-address>
                </neighbor>
            </bgp>
        </router>
    </configure>
  </config>
</edit-config>
EOF
```

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --rpc=del-conf-bgp-peer.xml
        <?xml version='1.0' encoding='UTF-8'?>
        <rpc-reply xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="urn:uuid:cb84a38d-905b-4117-85ba-2f108698f7b1">
            <ok/>
        </rpc-reply>

    *[gl:/configure router "Base" bgp]
    SROS# compare
    -   neighbor "172.31.22.5" {
    -       group "eBGPv4-EPE"
    -       peer-as 64505
    -   }

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123 --discard
        <?xml version='1.0' encoding='UTF-8'?>
        <ok xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0"/>

### Order of 'rpc' and 'commit' is important!

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123  --rpc=del-conf-bgp-peer.xml --commit
        <?xml version='1.0' encoding='UTF-8'?>
        <rpc-reply xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="urn:uuid:ed626db6-e371-4eee-b09a-989841c1caa9">
            <ok/>
        </rpc-reply>
        <?xml version='1.0' encoding='UTF-8'?>
        <ok xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0"/>

### GET STATE for BGP peer

```sh
cat > get-state-bgp-peer.xml <<EOF
<get>
    <filter>
        <state xmlns="urn:nokia.com:sros:ns:yang:sr:state" xmlns:nokia-attr="urn:nokia.com:sros:ns:yang:sr:attributes">
            <router>
                <router-name>Base</router-name>
                <bgp>
                    <neighbor>
                        <ip-address>172.31.22.1</ip-address>
                        <statistics>
                            <session-state/>
                            <last-established-time/>
                        </statistics>
                    </neighbor>
                </bgp>
            </router>
        </state>
  </filter>
</get>
EOF
```

    netconf-console2 --host 138.203.18.219 --port 830 -u netconf -p @nokia123  --rpc=get-state-bgp-peer.xml
        <?xml version='1.0' encoding='UTF-8'?>
        <rpc-reply xmlns:nc="urn:ietf:params:xml:ns:netconf:base:1.0" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0" message-id="urn:uuid:ca8ad1f8-6311-4a79-af5e-3a06dc75a39e">
            <data>
                <state xmlns="urn:nokia.com:sros:ns:yang:sr:state">
                    <router>
                        <router-name>Base</router-name>
                        <bgp>
                            <neighbor>
                                <ip-address>172.31.22.1</ip-address>
                                <statistics>
                                    <session-state>Established</session-state>
                                    <last-established-time>2022-05-20T07:42:07.5Z</last-established-time>
                                </statistics>
                            </neighbor>
                        </bgp>
                    </router>
                </state>
            </data>
        </rpc-reply>