Only "*.conf" files in this directory are processed for gRPC subscription .
If some files need to be excluded, then simple change of a file extension does a job.

Extensive directory structure can be created (per host or per subscription) to make it more manageable and scalable
