## Deployments steps

### Install Docker

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce

### Install docker-compose

    curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose

### Add flowcharting plugin to Grafana

> Check: <https://algenty.github.io/flowcharting-repository/ARCHIVES.html>

> Check: <https://github.com/algenty/grafana-flowcharting>

    mkdir -p ./telemetry/grafana/plugins/
    cd ./telemetry/grafana/plugins/
    wget https://algenty.github.io/flowcharting-repository/archives/agenty-flowcharting-panel-1.0.0a-SNAPSHOT.zip
    unzip agenty-flowcharting-panel-1.0.0a-SNAPSHOT.zip

### Install SNMP toolset

    apt install snmp

### Install gNMIc (Optional)

> Check: <https://gnmic.kmrd.dev/install/>

    bash -c "$(curl -sL <https://get-gnmic.kmrd.dev>)"

## Required configuration on every node

### [SROS] Configure NTP [Classic CLI example]

    /configure system time
        ntp
            server 138.203.19.135 router management prefer
            no shutdown
        exit
        dst-zone CEST
            start last sunday march 02:00
            end last sunday october 03:00
        exit
        zone CET

### [SROS] Configure NTP [MD-CLI example]

    /configure system time zone { standard name cet }
    /configure system time dst-zone "CEST" { end week last }
    /configure system time dst-zone "CEST" { end month october }
    /configure system time dst-zone "CEST" { end hours-minutes "03:00" }
    /configure system time dst-zone "CEST" { start week last }
    /configure system time dst-zone "CEST" { start month march }
    /configure system time dst-zone "CEST" { start hours-minutes "02:00" }
    /configure system time ntp { admin-state enable }
    /configure system time ntp { server 138.203.19.135 router-instance "management" prefer true }

### [SROS] Enable gRPC [MD-CLI example]

    /configure system { grpc admin-state enable }
    /configure system { grpc allow-unsecure-connection }

### [SROS] Add user with gRPC access [MD-CLI example]

	/configure global
	/configure system security user-params local-user password { complexity-rules allow-user-name true }
	commit
	/configure system security user-params local-user user "nokia" { password @nokia123 }
	/configure system security user-params local-user user "nokia" { access grpc true }
	/configure system security user-params local-user user "nokia" { console member ["administrative"] }
	commit

## Start/Stop

Simply use '01.start.sh' and '99.stop.sh' scripts to deploy/destroy TPG containers

> Note: If you plan to use  'netconf-console', 'gnmic' or 'snmp' only, then deployment of Telegraf/Prometeus/Grafana is not required!

## Grafana access

<http://<ip_address>:3000>

Login/password: admin/admin

### Telegraf tuning

Telegraf is a powerful tool to subscribe, convert and modify streaming data. It can also be used to perform SNMP polling.

Main configuration file 'telemetry/telegraf.conf' has to be adapted in order to fit your requirements:

- Subscription modes (sample, on-change, once) and XPATH
- Sample intervals, where it's applicable
- SNMP enabled/disabled
- Target IP Addresses and Ports of the nodes

> Note: Don't forget to define "hostname" in /etc/hosts file

    cat /etc/hosts | grep sros.node.ipd.lab
    138.203.18.19 sros.node.ipd.lab

Each modification of the configuration file requires a container restart.

> Note: selected by Telegraf timers are applicable for demo purposes and have to be carefully selected for the production!

