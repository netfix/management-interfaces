[root@compute-02 sros_MIBs]# grep Wattage *
numbers.txt:1.3.6.1.4.1.6527.3.1.2.2.1.13.1.6           tmnxHwResourceCurrentWattage                         LEAF  Integer32
numbers.txt:1.3.6.1.4.1.6527.3.1.2.2.1.13.1.7           tmnxHwResourcePeakWattage                            LEAF  Integer32
numbers.txt:1.3.6.1.4.1.6527.3.1.2.2.1.13.1.8           tmnxHwResourcePeakWattageTime                        LEAF  TimeStamp
numbers.txt:1.3.6.1.4.1.6527.3.1.2.2.1.13.1.9           tmnxHwResourceMinWattage                             LEAF  Integer32
numbers.txt:1.3.6.1.4.1.6527.3.1.2.2.1.13.1.10          tmnxHwResourceMinWattageTime                         LEAF  TimeStamp
numbers.txt:1.3.6.1.4.1.6527.3.1.2.2.1.13.1.16          tmnxHwResourceMaxRequiredWattage                     LEAF  Integer32



tmnxHwResourceCurrentWattage     OBJECT-TYPE
    SYNTAX      Integer32
    MAX-ACCESS  read-only
    STATUS      current
    DESCRIPTION
        "The value of tmnxHwResourceCurrentWattage indicates the current power
         use of the managed hardware component in milli-Watts (mW)."
    ::= { tmnxHwResourceEntry 6 }

# snmptranslate -IR -On TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550914
.1.3.6.1.4.1.6527.3.1.2.2.1.13.1.6.1.184550914


# snmpwalk -v 2c -c private 138.203.26.168 TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage
# Chassis/Fan/Etc
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663297 = INTEGER: 68109
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663298 = INTEGER: 67373
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663299 = INTEGER: 126324
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663300 = INTEGER: 85597
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663301 = INTEGER: 93144
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663302 = INTEGER: 76209
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663303 = INTEGER: 84216
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.100663304 = INTEGER: 75380
#Card
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.134217729 = INTEGER: 299823
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.134217730 = INTEGER: 306878
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.134217731 = INTEGER: 400940
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.134217732 = INTEGER: 496178
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.134217733 = INTEGER: 495590
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.134217734 = INTEGER: 485008
#CPM
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.150995057 = INTEGER: 277470
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.150995073 = INTEGER: 269169
#SFM
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772161 = INTEGER: 184777
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772162 = INTEGER: 187000
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772163 = INTEGER: 178111
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772164 = INTEGER: 181444
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772165 = INTEGER: 188111
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772166 = INTEGER: 188111
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772167 = INTEGER: 183666
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.167772168 = INTEGER: 183666
# MDA
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184549633 = INTEGER: 738942
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184549634 = INTEGER: 673668
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184549889 = INTEGER: 720665
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184549890 = INTEGER: 681501
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550145 = INTEGER: 1409960
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550146 = INTEGER: 723276
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550401 = INTEGER: 1443903
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550402 = INTEGER: 1276801
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550657 = INTEGER: 1454347
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550658 = INTEGER: 1268968
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550913 = INTEGER: 1086201
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.184550914 = INTEGER: 1430848
#?
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402653441 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402653442 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402653697 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402653698 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402653953 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402653954 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402654209 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402654210 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402654465 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402654466 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402654721 = INTEGER: 0
TIMETRA-CHASSIS-MIB::tmnxHwResourceCurrentWattage.1.402654722 = INTEGER: 0

